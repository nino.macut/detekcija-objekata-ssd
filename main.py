import cv2 as cv
import numpy as np

sigurnostDetekcije = 0.5 #manja vrijednost znaci manja preciznost
sigurnostDetekcijeNMSa = 0.2 #manja vrijednost znaci manje kutija

#postavke kamere
kamera = cv.VideoCapture(0) #0 je id kamere
kamera.set(3, 640) #širina
kamera.set(4, 480) #visina
kamera.set(10, 150) #svjetlost

imenaKlasa = []
putanjaDoKlasa = 'coco.names'
putanjaDoKonfiguracija = 'ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'
putanjaDoWeightova = 'frozen_inference_graph.pb'

#čitanje datoteke coco.names i spremanje naziva klasi u array
with open(putanjaDoKlasa, 'rt') as dat:
    imenaKlasa = dat.read().rstrip('\n').split('\n')

#kreiranje i konfiguracija modela detekcije objekata
modelDetekcije = cv.dnn_DetectionModel(putanjaDoWeightova, putanjaDoKonfiguracija)
modelDetekcije.setInputSize(320, 320)
modelDetekcije.setInputScale(1.0/127.5)
modelDetekcije.setInputMean((127.5, 127.5, 127.5))
modelDetekcije.setInputSwapRB(True)

while True:
    uspjeh, slika = kamera.read()
    #confThreshold određuje sigurnost sa kojom možemo reči da je detektiran objekt (0.5 = 50%)
    IDklasa, sigurnosti, boundingbox = modelDetekcije.detect(slika, confThreshold=sigurnostDetekcije)
    print(IDklasa, boundingbox)

    boundingbox = list(boundingbox)
    sigurnosti = np.array(sigurnosti).reshape(1, -1)[0]
    sigurnosti = list(map(float, sigurnosti))

    indeksi = cv.dnn.NMSBoxes(boundingbox, sigurnosti, sigurnostDetekcije, nms_threshold=sigurnostDetekcijeNMSa)
    print(indeksi)

    for i in indeksi:
        i = i[0] #makni uglatu zagradu
        box = boundingbox[i] #pronađi okvir pod navedenim indeksom
        x, y, width, height = box[0], box[1], box[2], box[3] #dimenzije okvira

        cv.rectangle(slika, (x,y), (x+width, y+height), color=(0, 255, 0), thickness=2) #stvara okvir oko detektiranog objekta

        cv.putText(slika, imenaKlasa[IDklasa[i][0] - 1].upper(), (x + 10, y + 30), #IDklase - 1 jer array pocinje od nule. (x + 10, y + 30) određuje poziciju texta
                   cv.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 2) #font, veličina fonta, boja fonta, debljina fonta
        cv.putText(slika, str(round(sigurnosti[i] * 100, 2)).upper() + "%", (x + 10, y + 60), #(x + 10, y + 30) određuje poziciju texta
                    cv.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 2) #font, veličina fonta, boja fonta, debljina fonta

    cv.imshow("Izlaz", slika)
    cv.waitKey(1)